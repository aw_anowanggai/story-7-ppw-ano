from django.shortcuts import render, redirect
from .models import *
from .forms import *

# Create your views here.

def index(request):
    statuses = Status.objects.all()
    status_form = StatusInputForm()
    if request.method == 'GET':
        return render(request, 'index.html', {'statuses' : statuses, 'status_form' : status_form})

    if request.method == 'POST':
        status_input = StatusInputForm(request.POST)
        if status_input.is_valid():
            status = Status(name = status_input.data['name'], message = status_input.data['status_message'])
            status.save()
            return render(request, 'index.html', {'statuses' : statuses, 'status_form': status_form})
        else:
            return render(request, 'index.html', {'statuses' : statuses, 'status_form': status_form})


def confirm(request):
    if request.method == 'POST':
        status_input = StatusInputForm(request.POST)
        if status_input.is_valid():
            confirm_name = status_input.data['name']
            confirm_status = status_input.data['status_message']
            confirm_form = StatusInputForm(initial={ 'name': confirm_name, 'status_message':confirm_status})
            return render(request, 'confirm.html', {'confirm_form' : confirm_form})
        else:
            response = redirect('/')
            return response
    if request.method == 'GET':
        return redirect('/')